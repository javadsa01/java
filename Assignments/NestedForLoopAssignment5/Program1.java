import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows");
		int row = Integer.parseInt(br.readLine());
		
		char ch = 64;
		ch+=row;
	        int num = row;
		
		for(int i=1; i<=row; i++) {
		
			for(int j=1; j<=row; j++) {
			
				System.out.print(ch+""+num+" ");
				if(i%2==0 && j!=row) {
				
					ch++;
					num++;
				} else if(i%2!=0 && j!=row) {
				
					ch--;
					num--;
				}
			}
			System.out.println();
		}
	}
}
