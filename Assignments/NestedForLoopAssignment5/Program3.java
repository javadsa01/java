
import java.io.*;

class Demo {

	public static void main(String[]args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter number of rows");
		int row = Integer.parseInt(br.readLine());
		
		int num = row;
		for(int i=1; i<=row; i++) {
			
			int num1 = i*num;
			num--;	
			for(int j=1; j<=row+1-i; j++) {
			
				System.out.print(num1+"\t");
				num1-=i;
			}
			System.out.println();
		}
	}
}
